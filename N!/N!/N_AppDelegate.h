//
//  N_AppDelegate.h
//  N!
//
//  Created by aluno on 8/30/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class N_ViewController;

@interface N_AppDelegate : NSObject <UIApplicationDelegate> {

}

@property (nonatomic, retain) IBOutlet UIWindow *window;

@property (nonatomic, retain) IBOutlet N_ViewController *viewController;

@end
